//
//  PhotoData.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/19/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import ObjectMapper

class PhotoData: Mappable {
    
    var sizes: PhotoSizes?
    var stat: Any?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        sizes   <- map ["sizes"]
        stat    <- map ["stat"]
    }
    
}


class PhotoSizes: Mappable {
    var canBlog: Any?
    var canPrint: Any?
    var canDownload: Any?
    var size: [Photo]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        canBlog      <- map["canblog"]
        canPrint     <- map["canprint"]
        canDownload  <- map["candownload"]
        size         <- map["size"]
    }
    
}


class Photo: Mappable {
    
    var label: Any?
    var width: Int?
    var height: Int?
    var source: String?
    var url: String?
    var media: Any?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        label   <- map["label"]
        width   <- map["width"]
        height  <- map["height"]
        source  <- map["source"]
        url     <- map["url"]
        media   <- map["media"]
    }
    
}
