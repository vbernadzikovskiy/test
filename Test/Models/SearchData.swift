//
//  SearchData.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/19/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import ObjectMapper

class SearchData: Mappable {
    
    var photos: SearchResult?
    var stat: Any?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        photos  <- map ["photos"]
        stat    <- map ["stat"]
    }
}

class SearchResult: Mappable {
    var page: Any?
    var pages: Any?
    var perPage: Any?
    var total: String?
    var photo: [SearchPhoto]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        page    <- map["page"]
        pages   <- map["pages"]
        perPage <- map["perpage"]
        total   <- map["total"]
        photo   <- map["photo"]
    }
}

class SearchPhoto: Mappable {
    
    var id: String?
    var owner: Any?
    var secret: Any?
    var farm: Any?
    var title: Any?
    var isPublic: Any?
    var isFriend: Any?
    var isFamily: Any?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id          <- map["id"]
        owner       <- map["owner"]
        secret      <- map["secret"]
        farm        <- map["farm"]
        title       <- map["title"]
        isPublic    <- map["ispublic"]
        isFriend    <- map["isFriend"]
        isFamily    <- map["isFamily"]
    }
}
