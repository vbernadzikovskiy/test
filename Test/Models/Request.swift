//
//  Request.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/19/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import RealmSwift

class Request: Object {
    @objc var requestString = ""
    @objc var urlString: String? = nil
    
    init(request: String, url: String) {
        self.requestString = request
        self.urlString = url
    }
    
    required init() {}
}
