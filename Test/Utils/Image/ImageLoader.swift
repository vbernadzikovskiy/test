//
//  ImageLoader.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/19/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import Foundation
import UIKit

final class ImageLoader {
    
    private let cache = ImageCache()
    
    func loadImage(from url: URL, completion: @escaping (UIImage?) -> Void) {
        
        if let image = cache[url] {
            completion(image)
        }
        else {
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                guard error == nil else { return }
                
                guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else { return }
                
                guard let data = data else { return }
                
                let image = UIImage(data: data)
                
                self.cache[url] = image
                completion(image)
                
            }.resume()
        }
    }
    
    func clearCache() {
        self.cache.removeAllImages()
    }
}
