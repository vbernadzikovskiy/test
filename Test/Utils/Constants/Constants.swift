//
//  Constants.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/19/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

struct ApiMethods {
    static let search = "https://api.flickr.com/services/rest/?method=flickr.photos.search"
    static let getSizes = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes"
}

struct ApiParameters {
    static let apiKey = "&api_key=1172886ca9e48f5bc0ebba8c784611e7"
    static let jsonFormat = "&format=json"
}
