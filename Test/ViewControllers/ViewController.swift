//
//  ViewController.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/19/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    
    let searchCellHeight: CGFloat = 60
    let historyTitleCellHeight: CGFloat = 50
    let historyCellHeight: CGFloat = 120
    
    let imageLoader = ImageLoader()
    fileprivate let imageRealm = try! Realm()
    
    let tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView() {
        
        view.addSubview(tableView)
        tableView.allowsSelection = false
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: UIApplication.shared.statusBarFrame.height).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        tableView.register(SearchTableViewCell.self, forCellReuseIdentifier: "SearchTableViewCell")
        tableView.register(HistoryTitleTableViewCell.self, forCellReuseIdentifier: "HistoryTitleTableViewCell")
        tableView.register(HistoryTableViewCell.self, forCellReuseIdentifier: "HistoryTableViewCell")
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    func sendImageNotFound() {
        let alert = UIAlertController(title: "Image not found!", message: "There is no such images on server...", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageRealm.objects(Request.self).count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as! SearchTableViewCell
            
            cell.searchHandler = { str in
                ApiManager.searchImageUrl(for: str, completion: { result in
                    if result == "" {
                        self.sendImageNotFound()
                    } else {
                        DispatchQueue.main.async {
                            try! self.imageRealm.write {
                                self.imageRealm.add(Request(request: str, url: result))
                            }
                            
                            tableView.reloadData()
                        }
                    }
                })
            }
            
            cell.setupViews()
            return cell
            
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTitleTableViewCell", for: indexPath) as! HistoryTitleTableViewCell
            cell.setupViews()
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
            cell.setupViews()
            
            let data = self.imageRealm.objects(Request.self)[imageRealm.objects(Request.self).count + 1 - indexPath.row]
            
            cell.requestLabel.text = data["requestString"] as? String
            
            guard let url = URL(string: data["urlString"] as? String ?? "") else { return cell }
            
            
            self.imageLoader.loadImage(from: url, completion: { image in
                DispatchQueue.main.async {
                    cell.responseImageView.image = image
                }
            })
            
            return cell
        }
    }
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return searchCellHeight
        } else if indexPath.row == 1 {
            return historyTitleCellHeight
        } else {
            return historyCellHeight
        }
    }
}
