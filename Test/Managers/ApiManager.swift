//
//  ApiManager.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/19/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import Foundation

class ApiManager {
    
    static func searchImageUrl(for string: String, completion: @escaping (String) -> Void) {
        
        guard let searchURL = generateUrl(method: ApiMethods.search, params: ["text" : string, "media":"photos"]) else { return }
        
        let session = URLSession.shared
        session.dataTask(with: searchURL) { (searchResult, searchResponse, searchError) in
    
            guard searchError == nil else { return }
            
            guard let searchResponse = searchResponse as? HTTPURLResponse, (200...299).contains(searchResponse.statusCode) else { return }
            
            guard let searchResult = searchResult else { return }
            
            let searchResultString = String(decoding: searchResult, as: UTF8.self).transformedForJson
            
            let searchData = SearchData(JSONString: searchResultString)
            
            if let total = searchData?.photos?.total, total == "0" {
                print("here")
                completion("")
                return
            }
            
            if let photoId = searchData?.photos?.photo?.randomElement()?.id{
                
                guard let photoSizeUrl = generateUrl(method: ApiMethods.getSizes, params: ["photo_id":photoId]) else { return }
                
                
                session.dataTask(with: photoSizeUrl) { (photoResult, photoResponse, photoError) in
                    
                    guard photoError == nil else { return }
                    
                    guard let photoResponse = photoResponse as? HTTPURLResponse, (200...299).contains(photoResponse.statusCode) else { return }
                    
                    guard let photoResult = photoResult else { return }
                    let photoResultString = String(decoding: photoResult, as: UTF8.self).transformedForJson
                    
                    
                    let photoData = PhotoData(JSONString: photoResultString)
                    
                    guard let urlResult = photoData?.sizes?.size?[3].source else { return }
                    completion(urlResult)
                    
                    }.resume()
                
                } else { return }
            
            }.resume()
    }
    
    private class func generateUrl(method: String, params: Dictionary<String, String>) -> URL? {
        var requestString = ""
        requestString += method + ApiParameters.apiKey
        
        params.forEach { key, value in
            requestString += "&" + key + "=" + value
        }
        
        requestString += ApiParameters.jsonFormat
        
        return URL(string: requestString)
    }
}
