//
//  HistoryTitleTableViewCell.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/20/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import UIKit

class HistoryTitleTableViewCell: BaseTableViewCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Search History"
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textColor = UIColor.hex("8b8b8d")
        return label
    }()
    
    let higherBorderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.hex("bfbfbf")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    override func setupViews() {
        self.backgroundColor = UIColor.hex("f7f7f7")
        
        addSubview(titleLabel)
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9).isActive = true
        titleLabel.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.9).isActive = true
        
        addSubview(higherBorderView)
        higherBorderView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        higherBorderView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        higherBorderView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        higherBorderView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
}
