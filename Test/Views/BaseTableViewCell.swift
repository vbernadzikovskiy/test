//
//  BaseCellProtocol.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/20/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    func setupViews() {}
    func reloadContent() {}
    
    override func prepareForReuse() {
        reloadContent()
    }
}
