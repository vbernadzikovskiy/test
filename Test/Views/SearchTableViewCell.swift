//
//  SearchTableViewCell.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/20/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import UIKit


class SearchTableViewCell: BaseTableViewCell {
    
    let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.barTintColor = UIColor.hex("f7f7f7")
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.hex("e9e9e9")
        searchBar.backgroundImage = UIImage()
        searchBar.placeholder = "Search for images"
        searchBar.showsCancelButton = true
        return searchBar
    }()
    
    var searchHandler: (String) -> () = { str in
        print(str)
    }
    
    override func setupViews() {
        
        self.backgroundColor = UIColor.hex("f7f7f7")
        
        addSubview(searchBar)
        searchBar.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        searchBar.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        searchBar.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        searchBar.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.8).isActive = true
        searchBar.delegate = self

    }
    
    func reloadSearchBar() {
        searchBar.text = ""
        searchBar.endEditing(true)
    }
}

extension SearchTableViewCell: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        reloadSearchBar()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            searchHandler(text)
        }
        
        reloadSearchBar()
    }
}
