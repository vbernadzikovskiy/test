//
//  HistoryTableViewCell.swift
//  Test
//
//  Created by Bernadzikovskiy Slavik on 3/20/20.
//  Copyright © 2020 Bernadzikovskiy Slavik. All rights reserved.
//

import UIKit

class HistoryTableViewCell: BaseTableViewCell {
    
    let requestLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Search History"
        label.textAlignment = .center
        return label
    }()
    
    let responseImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .red
        return imageView
    }()
    
    let borderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.hex("bfbfbf")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func setupViews() {
        
        addSubview(responseImageView)
        responseImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        responseImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5).isActive = true
        responseImageView.widthAnchor.constraint(equalTo: responseImageView.heightAnchor, multiplier: 2).isActive = true
        responseImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        
        
        addSubview(requestLabel)
        requestLabel.leftAnchor.constraint(equalTo: responseImageView.rightAnchor, constant: 10).isActive = true
        requestLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        requestLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        requestLabel.heightAnchor.constraint(equalTo: widthAnchor, multiplier: 0.8).isActive = true
        
        addSubview(borderView)
        borderView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        borderView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        borderView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        borderView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
}
